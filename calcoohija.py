#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):
    """
    Clase calculadora hija de la que heredamos
    las funciones de la madre
    """

    def mult(self):
        """
        Operacion multiplicar
        """
        return self.operando1 * self.operando2

    def div(self):
        """
        Operacion division
        """
        if self.operando2 == 0:
            sys.exit("Division by zero is not allowed")
        else:
            return self.operando1 / self.operando2

    def operar(self):
        """
        añadimos la operacion multiplicacion y division
        """
        result = calcoo.Calculadora.operar(self)
        if not result:
            if self.operador == "multiplica":
                result = self.mult()
            elif self.operador == "divide":
                result = self.div()
            else:
                sys.exit("Operacion no soportada")
        return result


if __name__ == "__main__":
    calculadoraHija = CalculadoraHija("operador", 0, 0)

    result = calculadoraHija.operar()

    print(result)
