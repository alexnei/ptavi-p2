#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calculadora():
    """
    Clase madre Calculadora
    """

    # Definimos los metodos de nuestra clase.
    def __init__(self, operador, operando1, operando2):
        """
        inicializa las variables de la calculadora
        """
        self.operador = operador
        self.operando1 = operando1
        self.operando2 = operando2

    def suma(self):
        """
        Operacion suma
        """
        return self.operando1 + self.operando2

    def resta(self):
        """
        Operacion resta
        """
        return self.operando1 - self.operando2

    def operar(self):
        """
        metodo para operar entre los dos numeros de la calculadora
        """
        try:
            self.operando1 = int(sys.argv[1])
            self.operando2 = int(sys.argv[3])
            self.operador = str(sys.argv[2])
        except ValueError:
            sys.exit("Error: non numerical numbers")

        if self.operador == "suma":
            result = self.suma()
        elif self.operador == "resta":
            result = self.resta()
        else:
            return None
        return result


if __name__ == "__main__":
    calculadora = Calculadora("operador", 0, 0)

    result = calculadora.operar()
    if not result:
        result = ("Operacion solo puede ser suma o resta")
    print(result)
